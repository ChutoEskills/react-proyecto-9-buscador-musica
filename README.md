# React Proyecto 9 - Buscador Musica
Obtención de letras de canciones e información del artista

[Link to Web:](https://gifted-hoover-7a406f.netlify.app/)

## ¿Herramientas utilizadas?
- Bootswatch
- useState 
- lyrics.ovh API
- theaudiodb API
- Ajax Request/Promises


## ¿Cómo funciona?
- Se crea el formulario para recolectar el nombre del grupo o artista y su canción, luego esa información se agrega a la URL para consumir las 2 API mediante GET, una vez que se obtiene la información se procede a colocarla en cada uno de los apartados que se maquetaron.

## ¿Dudas?
